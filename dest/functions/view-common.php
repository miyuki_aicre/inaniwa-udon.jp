<?php 

/**
 * memo func
 **/
function memo(){
	/*---------------------------------------*/
	/* navigation */
	/*---------------------------------------*/
	$list = wp_get_nav_menu_items('global_navigation');
	if(!$list) return '<p>global_navigationメニューが設定されていません。</p>';


	/*---------------------------------------*/
	/* transient */
	/*---------------------------------------*/
	//- キー名
	$transient_key = 'news_archive_array';
	//- for debug
	// delete_transient($transient_key);
	//- fetch
	if(false === ($_result = get_transient($transient_key))){

		/*---------------------------------------*/
		/* set transient */
		/*---------------------------------------*/
		set_transient($transient_key,$_result,24*HOUR_IN_SECONDS);
	}


	/*---------------------------------------*/
	/* query */
	/*---------------------------------------*/
	$args = [
		'posts_per_page' => 5,
		'post_type' => 'faq',
		'tax_query' => [
			[
				'taxonomy' => 'faq-types',
				'field' => 'slug',
				'terms' => $term->slug,
			]
		],
	];
	$posts = get_posts($args);


	/*---------------------------------------*/
	/* thumbnail */
	/*---------------------------------------*/
	//- easy to use
	$thumbnail_url = get_the_post_thumbnail_url($item->ID,'large');
	//- or this is core
	$post_thumbnail_id = get_post_thumbnail_id( $post );
	if ( ! $post_thumbnail_id ) {
		return false;
	}
	return wp_get_attachment_image_url( $post_thumbnail_id, $size );



	/*---------------------------------------*/
	/* get page by slug */
	/*---------------------------------------*/
	$newsPage = get_page_by_path('news');

}






