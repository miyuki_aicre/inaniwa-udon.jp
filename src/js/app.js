//- dotenv
// process.env.APPKEY

/* --------------------------------------- */
/* import modules */
/* --------------------------------------- */
// import Vue from 'vue';
// import * as types from './mutation-types';
// import store from './store';
// import router from './router';

/* --------------------------------------- */
/* コンポーネント */
/* --------------------------------------- */
// import GlobalHeader from '../vue/global-header.vue';

/* --------------------------------------- */
/* 初期設定 */
/* --------------------------------------- */
var init = () => {
	/* --------------------------------------- */
	/* フィルター登録 */
	/* --------------------------------------- */
	// Vue.filter('currency', (value) => {
	// 	const currency = value;
	// 	return currency.toString().replace(/(\d)(?=(\d{3})+$)/g, '$1,')
	// });

	/* --------------------------------------- */
	/* app生成 */
	/* --------------------------------------- */
	// const app = new Vue({
	// 	store,
	// 	router,
	// 	components: {
	// 		GlobalHeader,
	// 	},
	// }).$mount('#app')

}

document.addEventListener('DOMContentLoaded', init)
